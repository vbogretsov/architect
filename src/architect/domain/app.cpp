#include <iostream>
#include <vector>
#include <algorithm>

#include "../utils/tree.h"
#include "../utils/iterable.h"
#include "uml_class.h"

using architect::domain::uml_package;
using architect::domain::uml_package_ptr;

shared_ptr<uml_package> create_tree()
{
    auto system = uml_package::create("System");
    auto net = uml_package::create("Net");
    auto collections = uml_package::create("Collections");
    auto generic = uml_package::create("Generic");
    auto concurent = uml_package::create("Concurent");
    auto specialized = uml_package::create("Specialized");
    auto codedom = uml_package::create("CodeDom");
    auto configuration = uml_package::create("Configuration");
    auto xml = uml_package::create("Xml");
    auto linq = uml_package::create("Linq");
    system->add(collections);
    system->add(net);
    system->add(codedom);
    system->add(configuration);
    system->add(xml);
    collections->add(generic);
    collections->add(specialized);
    collections->add(concurent);
    xml->add(linq);
    return system;
}

void write_line(const std::string& s, int indent)
{
    for (int i = 0; i < indent; i++)
    {
        std::cout << ' ';
    }
    std::cout << s << std::endl;
}

class package_visitor : public uts::tree_visitor<uml_package_ptr>
{
public:
    package_visitor(int indent_size)
        : _indent(0), _indent_size(indent_size)
    {
    }
    void up() override
    {
        _indent -= _indent_size;
    }
    void down() override
    {
        _indent += _indent_size;
    }
    void visit(uml_package_ptr& package) override
    {
        write_line(*(package->full_name("::")), _indent);
    }
private:
    int _indent;
    int _indent_size;
};

void test()
{
    auto system = create_tree();
    auto children_selector = [](uml_package_ptr& p) { return p->packages(); };
    uts::tree<uml_package_ptr> t(system, children_selector);
    package_visitor visitor(4);
    t.bypass(&visitor);
}

void test2()
{
    shared_ptr<uml_package> p(new uml_package("System"));
}

class A
{
public:
    ~A();
    virtual void f();
};

A::~A()
{
    std::cout << "~A" << std::endl;
}

void A::f()
{
    std::cout << "A::f()" << std::endl;
}

class B : public A
{
public:
    ~B();
    void f();
};

B::~B()
{
    std::cout << "~B" << std::endl;
}

void B::f()
{
    std::cout << "B::f()" << std::endl;
}

void test3()
{
    std::shared_ptr<A> a(new B());
    a->f();
}

int main(int argc, char const argv[])
{
    test();
    //test3();
    getchar();
    return 0;
}