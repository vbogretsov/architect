#include "uml_association.h"

namespace architect
{
    namespace domain
    {
        namespace relations
        {
            class uml_association::pimpl
            {
            public:

                pimpl(uml_association_type type)
                    : _type(type),
                    _source_multiplicity(uml_multiplicity::SINGLE),
                    _target_multiplicity(uml_multiplicity::SINGLE)
                {
                }

                uml_association_type type() const
                {
                    return _type;
                }

                void type(uml_association_type value)
                {
                    _type = value;
                }

                uml_multiplicity source_multiplicity() const
                {
                    return _source_multiplicity;
                }

                void source_multiplicity(uml_multiplicity value)
                {
                    _source_multiplicity = value;
                }

                uml_multiplicity target_multiplicity() const
                {
                    return _target_multiplicity;
                }

                void target_multiplicity(uml_multiplicity value)
                {
                    _target_multiplicity = value;
                }

            private:

                uml_multiplicity _source_multiplicity;

                uml_multiplicity _target_multiplicity;

                uml_association_type _type;
            };

            uml_association::uml_association(
                const std::string& name,
                uml_association_type type,
                uml_type_const_ptr source,
                uml_type_const_ptr target)
                : uml_relation(name, source, target), impl()
            {
            }

            uml_association::~uml_association()
            {
            }

            uml_association_type uml_association::type() const
            {
                return impl->type();
            }

            void uml_association::type(uml_association_type value)
            {
                impl->type(value);
            }

            uml_multiplicity uml_association::source_multiplicity() const
            {
                return impl->source_multiplicity();
            }

            void uml_association::sourcemultiplicity(uml_multiplicity value)
            {
                impl->source_multiplicity(value);
            }

            uml_multiplicity uml_association::target_multiplicity() const
            {
                return impl->target_multiplicity();
            }

            void uml_association::target_multiplicity(uml_multiplicity value)
            {
                impl->target_multiplicity(value);
            }
        }
    }
}