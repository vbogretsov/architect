#ifndef UML_ASSOCIATION_H
#define UML_ASSOCIATION_H

#include "../uml_def.h"
#include "uml_relation.h"

namespace architect
{
    namespace domain
    {
        namespace relations
        {
            // Represents the association relation
            class uml_association : public uml_relation
            {
            public:
                // Creates new instance of uml_association
                uml_association(
                    const std::string& name,
                    uml_association_type type,
                    uml_type_const_ptr source,
                    uml_type_const_ptr target);
                // Deletes resources
                ~uml_association();
                // Gets type of the association
                uml_association_type type() const;
                // Sets type of the association
                void type(uml_association_type value);
                // Gets multiplicity of the source type
                uml_multiplicity source_multiplicity() const;
                // Sets multiplicity of the source type
                void sourcemultiplicity(uml_multiplicity value);
                // Gets multiplicity of the target type
                uml_multiplicity target_multiplicity() const;
                // Sets multiplicity of the target type
                void target_multiplicity(uml_multiplicity value);
            private:
                USING_PIMPL(uml_association)
            };
        }
    }
}

#endif