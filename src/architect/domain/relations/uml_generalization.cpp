#include "uml_generalization.h"

namespace architect
{
    namespace domain
    {
        namespace relations
        {
            class uml_generalization::pimpl
            {
            public:

                pimpl(uml_generalization_type type)
                    : _type(type)
                {
                }

                uml_generalization_type type() const
                {
                    return _type;
                }

                void type(uml_generalization_type value)
                {
                    _type = value;
                }

            private:

                uml_generalization_type _type;
            };

            uml_generalization::uml_generalization(
                const std::string& name,
                uml_generalization_type type,
                uml_type_const_ptr source,
                uml_type_const_ptr target)
                : uml_relation(name, source, target)
            {
            }

            uml_generalization::~uml_generalization()
            {
            }

            uml_generalization_type uml_generalization::type() const
            {
                return impl->type();
            }

            void uml_generalization::type(uml_generalization_type value)
            {
                impl->type(value);
            }
        }
    }
}