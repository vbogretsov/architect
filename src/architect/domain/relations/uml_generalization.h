#ifndef UML_GENERALIZATION_H
#define UML_GENERALIZATION_H

#include "../uml_def.h"
#include "uml_relation.h"

namespace architect
{
    namespace domain
    {
        namespace relations
        {
            // Represents the generalization relation
            class uml_generalization : public uml_relation
            {
            public:
                // Creates new instance of uml_generalization
                uml_generalization(
                    const std::string& name,
                    uml_generalization_type type,
                    uml_type_const_ptr source,
                    uml_type_const_ptr target);
                // Deletes resources
                ~uml_generalization();
                // Gets type of generalization
                uml_generalization_type type() const;
                // Sets type of generalization
                void type(uml_generalization_type value);
            private:
                USING_PIMPL(uml_generalization)
            };
        }
    }
}

#endif