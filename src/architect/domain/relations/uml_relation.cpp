#include "uml_relation.h"

namespace architect
{
    namespace domain
    {
        namespace relations
        {
            class uml_relation::pimpl
            {
            public:

                pimpl(uml_type_const_ptr source, uml_type_const_ptr target)
                    : _source(source), _target(target)
                {
                }

                uml_type_const_ptr source() const
                {
                    return _source;
                }

                uml_type_const_ptr target() const
                {
                    return _target;
                }

            private:

                uml_type_const_ptr _source;
                uml_type_const_ptr _target;
            };

            uml_relation::uml_relation(
                const std::string& name,
                uml_type_const_ptr source,
                uml_type_const_ptr target)
                : uml_object(name), impl(new pimpl(source, target))
            {
            }

            uml_relation::~uml_relation()
            {
            }

            uml_type_const_ptr uml_relation::source() const
            {
                return impl->source();
            }

            uml_type_const_ptr uml_relation::target() const
            {
                return impl->target();
            }
        }
    }
}