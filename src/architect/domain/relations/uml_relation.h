#ifndef UML_RELATION_H
#define UML_RELATION_H

#include "../uml_type.h"

namespace architect
{
    namespace domain
    {
        namespace relations
        {
            // Represents base class for uml relations
            class uml_relation : public uml_object
            {
            public:
                // Clears all resources
                virtual ~uml_relation();
                // Gets source type
                uml_type_const_ptr source() const;
                // Gets target type
                uml_type_const_ptr target() const;
            protected:
                uml_relation(
                    const std::string& name,
                    uml_type_const_ptr source,
                    uml_type_const_ptr target);
            private:
                USING_PIMPL(uml_relation)
            };
        }
    }
}

#endif