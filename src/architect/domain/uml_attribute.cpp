#include "uml_attribute.h"

namespace architect
{
    namespace domain
    {
        class uml_attribute::pimpl
        {
        public:

            pimpl()
                : _multiplicity(uml_multiplicity::SINGLE)
            {
            }

            shared_ptr<const uml_type> type() const
            {
                return _type;
            }

            void type(shared_ptr<const uml_type> value)
            {
                _type = value;
            }

            uml_multiplicity multiplicity() const
            {
                return _multiplicity;
            }

            void multiplicity(uml_multiplicity value)
            {
                _multiplicity = value;
            }

        private:

            uml_multiplicity _multiplicity;

            shared_ptr<const uml_type> _type;
        };

        uml_attribute::uml_attribute(const std::string& name)
            : uml_object(name), impl(new pimpl())
        {
        }

        uml_attribute::~uml_attribute()
        {
        }

        shared_ptr<const uml_type> uml_attribute::type() const
        {
            return impl->type();
        }
        
        void uml_attribute::type(shared_ptr<const uml_type> value)
        {
            impl->type(value);
        }

        uml_multiplicity uml_attribute::multiplicity() const
        {
            return impl->multiplicity();
        }
        
        void uml_attribute::multiplicity(uml_multiplicity value)
        {
            impl->multiplicity(value);
        }
    }
}