#ifndef UML_ATTRIBUTE_H
#define UML_ATTRIBUTE_H

#include "uml_type.h"
#include "uml_def.h"

namespace architect
{
    namespace domain
    {
        // Represetns argument of the function
        class uml_attribute : public uml_object
        {
        public:
            // Creates new instance of uml_attribute
            uml_attribute(const std::string& name);
            // Clears all resources
            virtual ~uml_attribute();
            // Gets argument type
            uml_type_const_ptr type() const;
            // Sets argument type
            void type(uml_type_const_ptr value);
            // Gets multiplicity
            uml_multiplicity multiplicity() const;
            // Sets multiplicity
            void multiplicity(uml_multiplicity value);
        private:
            USING_PIMPL(uml_attribute)
        };

        typedef std::shared_ptr<uml_attribute> uml_attribute_ptr;
        typedef std::shared_ptr<const uml_attribute> uml_attribute_const_ptr;
    }
}

#endif