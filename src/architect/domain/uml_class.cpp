#include "uml_class.h"

namespace architect
{
    namespace domain
    {
        class  uml_class::pimpl
        {
        public:

            pimpl()
                : _is_abstract(false)
            {
            }

            uts::iterable<uml_member_ptr> attributes()
            {
                return uts::iterable<uml_member_ptr>(
                    _attributes.begin(),
                    _attributes.end());
            }

            void add(uml_member_ptr attribute)
            {
                _attributes.push_back(attribute);
            }

            void remove(uml_member_ptr attribute)
            {
                _attributes.remove(attribute);
            }

            bool is_abstract() const
            {
                return _is_abstract;
            }

            void is_abstract(bool value)
            {
                _is_abstract = value;
            }

        private:

            std::list<uml_member_ptr> _attributes;

            bool _is_abstract;
        };

        uml_class::uml_class(const std::string& name)
            : uml_interface(name), impl(new pimpl())
        {
        }

        uml_class::~uml_class()
        {
        }

        uts::iterable<uml_member_ptr>uml_class::attributes() const
        {
            return impl->attributes();
        }

        void uml_class::add(uml_member_ptr attribute)
        {
            impl->add(attribute);
        }

        void uml_class::remove(uml_member_ptr attribute)
        {
            impl->remove(attribute);
        }

        bool uml_class::is_abstract() const
        {
            return impl->is_abstract();
        }

        void uml_class::is_abstract(bool value) const
        {
            impl->is_abstract(value);
        }
    }
}