#ifndef UML_CLASS_H
#define UML_CLASS_H

#include "uml_interface.h"

namespace architect
{
    namespace domain
    {
        // Represents uml_class
        class uml_class : public uml_interface
        {
        public:
            // Creates new instance of uml_class
            uml_class(const std::string& name);
            // Deletes resources
            ~uml_class();
            // Gets list of attributes
            uts::iterable<uml_member_ptr> attributes() const;
            // Adds the attribute
            void add(uml_member_ptr attribute);
            // Removes the attribute
            void remove(uml_member_ptr attribute);
            // Gets true is the class is abstract, otherwise false
            bool is_abstract() const;
            // Sets abstract value
            void is_abstract(bool value) const;
        private:
            USING_PIMPL(uml_class)
        };

        typedef shared_ptr<uml_class> uml_class_ptr;
        typedef shared_ptr<const uml_class> uml_class_const_ptr;
    }
}

#endif