#ifndef UML_DEF_H
#define UML_DEF_H

namespace architect
{
    namespace domain
    {
        enum uml_visibility
        {
            PRIVATE,
            PROTECTED,
            PACKAGE,
            PUBLIC
        };

        enum uml_multiplicity
        {
            OPTIONAL,
            MONDATORY,
            SINGLE,
            MULTIPLE
        };

        enum uml_generalization_type
        {
            INHERITANCE,
            IMPLEMENTATION
        };

        enum uml_association_type
        {
            SIMPLE,
            DIRECT,
            BIDIRECT,
            DEPENDENCY,
            COMPOSITION,
            AGGREGATION
        };
    }
}

#endif