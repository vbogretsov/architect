#include "uml_interface.h"

namespace architect
{
    namespace domain
    {
        class uml_interface::pimpl
        {
        public:

            uts::iterable<uml_operation_ptr> operations()
            {
                return uts::iterable<uml_operation_ptr>(
                    _operations.begin(),
                    _operations.end());
            }

            void add(uml_operation_ptr operation)
            {
                _operations.push_back(operation);
            }

            void remove(uml_operation_ptr operation)
            {
                _operations.remove(operation);
            }

        private:

            std::list<uml_operation_ptr> _operations;
        };


        uml_interface::uml_interface(const std::string& name)
            : uml_type(name), impl(new pimpl())
        {
        }

        uml_interface::~uml_interface()
        {
        }

        uts::iterable<uml_operation_ptr> uml_interface::operations() const
        {
            return impl->operations();
        }

        void uml_interface::add(uml_operation_ptr operation)
        {
            impl->add(operation);
        }

        void uml_interface::remove(uml_operation_ptr operation)
        {
            impl->remove(operation);
        }
    }
}