#ifndef UML_INTERFACE_H
#define UML_INTERFACE_H

#include "uml_operation.h"

namespace architect
{
    namespace domain
    {
        // Represents the interface
        class uml_interface : public uml_type
        {
        public:
            // Creates new instance of uml_interface
            uml_interface(const std::string& name);
            // Clears all resources
            virtual ~uml_interface();
            // Gets operations list
            uts::iterable<uml_operation_ptr> operations() const;
            // Adds the operation
            void add(uml_operation_ptr operation);
            // Removes the operation
            void remove(uml_operation_ptr operation);
        private:
            USING_PIMPL(uml_interface)
        };

        typedef shared_ptr<uml_interface> uml_interface_ptr;
        typedef shared_ptr<const uml_interface> uml_interface_const_ptr;
    }
}

#endif