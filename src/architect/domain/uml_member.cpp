#include "uml_member.h"

namespace architect
{
    namespace domain
    {
        class uml_member::pimpl
        {
        public:

            pimpl()
                : _visibility(uml_visibility::PRIVATE), _is_static(true)
            {
            }

            uml_visibility visibility() const
            {
                return _visibility;
            }

            void visibility(uml_visibility value)
            {
                _visibility = value;
            }

            bool is_static() const
            {
                return _is_static;
            }

            void is_static(bool value)
            {
                _is_static = value;
            }

        private:

            uml_visibility _visibility;

            bool _is_static;
        };

        uml_member::uml_member(const std::string& name)
            : uml_attribute(name), impl(new pimpl())
        {
        }

        uml_member::~uml_member()
        {
        }

        bool uml_member::is_static() const
        {
            return impl->is_static();
        }
        
        void uml_member::is_static(bool value)
        {
            impl->is_static(true);
        }

        uml_visibility uml_member::visibility() const
        {
            return impl->visibility();
        }
        
        void uml_member::visibility(uml_visibility value)
        {
            impl->visibility(value);
        }
    }
}