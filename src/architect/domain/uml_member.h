#ifndef UML_MEMBER_H
#define UML_MEMBER_H

#include "uml_attribute.h"

namespace architect
{
    namespace domain
    {
        // Represents class member
        class uml_member : public uml_attribute
        {
        public:
            // Creates new instance of uml_member
            uml_member(const std::string& name);
            // Clears all resources
            virtual ~uml_member();
            // Gets true if the member is static otherwise false
            bool is_static() const;
            // Sets static property
            void is_static(bool value);
            // Gets visibility of the member
            uml_visibility visibility() const;
            // Sets visibility of the member
            void visibility(uml_visibility value);
        private:
            USING_PIMPL(uml_member)
        };

        typedef shared_ptr<uml_member> uml_member_ptr;
        typedef shared_ptr<const uml_member> uml_member_const_ptr;
    }
}

#endif