#include "uml_model.h"
#include "../utils/tree.h"

namespace architect
{
    namespace domain
    {
        class uml_model::pimpl
        {
        public:

            uts::iterable<uml_package_ptr> roots()
            {
                return uts::iterable<uml_package_ptr>(
                    _packages.begin(),
                    _packages.end());
            }

            uts::iterable<uml_package_ptr> packages()
            {
                throw;
            }

            uml_package_ptr create_package(
                uml_package_ptr parent,
                const std::string& name)
            {
                uml_package_ptr child(new uml_package(name));
                parent->add(child);
                return child;
            }

            uml_package_ptr create_package(
                const std::string& name)
            {
                 uml_package_ptr root(new uml_package(name));
                 _packages.push_back(root);
                 return root;
            }

            uml_interface_ptr create_interface(
                uml_package_ptr parent,
                const std::string& name)
            {
                uml_interface_ptr child(new uml_interface(name));
                parent->add(child);
                return child;
            }

            uml_class_ptr create_class(
                uml_package_ptr parent,
                const std::string& name)
            {
                uml_class_ptr child(new uml_class(name));
                parent->add(child);
                return child;
            }

            uml_operation_ptr create_operation(
                uml_interface_ptr parent,
                const std::string& name)
            {
                uml_operation_ptr child(new uml_operation(name));
                parent->add(child);
                return child;
            }

            uml_member_ptr create_member(
                uml_class_ptr parent,
                const std::string& name)
            {
                uml_member_ptr child(new uml_member(name));
                parent->add(child);
                return child;
            }

            uml_attribute_ptr add_parameter(
                uml_operation_ptr parent,
                const std::string& name)
            {
                uml_attribute_ptr child(new uml_attribute(name));
                parent->add(child);
                return child;
            }

        private:

            std::list<uml_package_ptr> _packages;
        };

        uml_model::uml_model(const std::string& name)
            : uml_object(name)
            , impl(new pimpl())
        {
        }

        uts::iterable<uml_package_ptr> uml_model::roots() const
        {
            return impl->roots();
        }

        uml_package_ptr uml_model::create_package(
            uml_package_ptr parent,
            const std::string& name)
        {
            return impl->create_package(parent, name);
        }

        uml_package_ptr uml_model::create_package(
            const std::string& name)
        {
            return impl->create_package(name);
        }

        uml_interface_ptr uml_model::create_interface(
            uml_package_ptr parent,
            const std::string& name)
        {
            return impl->create_interface(parent, name);
        }

        uml_class_ptr uml_model::create_class(
            uml_package_ptr parent,
            const std::string& name)
        {
            return impl->create_class(parent, name);
        }

        uml_operation_ptr uml_model::create_operation(
            uml_interface_ptr parent,
            const std::string& name)
        {
            return impl->create_operation(parent, name);
        }

        uml_member_ptr uml_model::create_member(
            uml_class_ptr parent,
            const std::string& name)
        {
            return impl->create_member(parent, name);
        }

        uml_attribute_ptr uml_model::add_parameter(
            uml_operation_ptr parent,
            const std::string& name)
        {
            return impl->add_parameter(parent, name);
        }
    }
}