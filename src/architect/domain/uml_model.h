#ifndef UML_MODEL_H
#define UML_MODEL_H

#include "uml_class.h"
#include "../utils/iterable.h"

namespace architect
{
    namespace domain
    {
        // Represents the application model
        class uml_model : public uml_object
        {
        public:
            // Creates new model
            uml_model(const std::string& name);
            // Gets model root namespaces
            uts::iterable<uml_package_ptr> roots() const;
            // Gets all packages
            uts::iterable<uml_package_ptr> packages() const;
            // Creates new class in the specified package
            uml_class_ptr create_class(
                uml_package_ptr parent,
                const std::string& name);
            // Creates new interface in the specified package
            uml_interface_ptr create_interface(
                uml_package_ptr parent,
                const std::string& name);
            // Creates new package in the specified package
            uml_package_ptr create_package(
                uml_package_ptr parent,
                const std::string& name);
            // Creates new root package
            uml_package_ptr create_package(
                const std::string& name);
            // Creates new operation in the specified interface
            uml_operation_ptr create_operation(
                uml_interface_ptr parent,
                const std::string& name);
            // Creates new attribute in the specified class
            uml_member_ptr create_member(
                uml_class_ptr parent,
                const std::string& name);
            // Creates new parameter for the specified operation
            uml_attribute_ptr add_parameter(
                uml_operation_ptr parent,
                const std::string& name);
        private:
            USING_PIMPL(uml_model)
        };
    }
}

#endif