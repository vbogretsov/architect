#include "uml_object.h"

#include <iostream>

namespace architect
{
    namespace domain
    {

        class uml_object::pimpl
        {
        public:

            pimpl(const std::string& name)
                : _name(name)
            {
            }

            ~pimpl()
            {
                std::cout << "DELETING " << _name << std::endl;
            }

            const std::string& name() const
            {
                    return _name;
            }

            void name(const std::string& value)
            {
                _name = value;
            }

        private:

            std::string _name;
        };

        uml_object::uml_object(const std::string& name)
            : impl(new pimpl(name))
        {
        }

        uml_object::~uml_object()
        {
        }

        const std::string& uml_object::name() const
        {
            return impl->name();
        }

        void uml_object::name(const std::string& value)
        {
            impl->name(value);
        }
    }
}