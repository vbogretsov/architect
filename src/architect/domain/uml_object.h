#ifndef UML_OBJECT_H
#define UML_OBJECT_H

#include <string>

#include "../utils/pimpl.h"

using std::unique_ptr;
using std::shared_ptr;

namespace architect
{
    namespace domain
    {
        // Base class for all UML objects
        class uml_object
        {
        public:
            virtual ~uml_object();
            // Gets object name
            const std::string& name() const;
            // Sets object name
            void name(const std::string& value);
        protected:
            // Creates new instance of uml_object
            uml_object(const std::string& name);
        private:
            USING_PIMPL(uml_object)
        };

        typedef std::shared_ptr<uml_object> uml_object_ptr;
        typedef std::shared_ptr<const uml_object> uml_object_const_ptr;
    }
}

#endif