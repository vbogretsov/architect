#include "uml_operation.h"

namespace architect
{
    namespace domain
    {
        class uml_operation::pimpl
        {
        public:

            pimpl()
                : _is_abstract(false)
            {
            }

            uts::iterable<uml_attribute_ptr> arguments()
            {
                return uts::iterable<uml_attribute_ptr>(
                    _arguments.begin(),
                    _arguments.end());
            }

            void add(uml_attribute_ptr argument)
            {
                _arguments.push_back(argument);
            }

            void remove(uml_attribute_ptr argument)
            {
                _arguments.remove(argument);
            }

            bool is_abstract() const
            {
                return _is_abstract;
            }

            void is_abstract(bool value)
            {
                _is_abstract = value;
            }

        private:

            std::list<uml_attribute_ptr> _arguments;

            bool _is_abstract;
        };


        uml_operation::uml_operation(const std::string& name)
            : uml_member(name), impl(new pimpl())
        {
        }

        uml_operation::~uml_operation()
        {
        }

        uts::iterable<uml_attribute_ptr> uml_operation::arguments() const
        {
            return impl->arguments();
        }

        void uml_operation::add(uml_attribute_ptr argument)
        {
            impl->add(argument);
        }

        void uml_operation::remove(uml_attribute_ptr argument)
        {
            impl->remove(argument);
        }

        bool uml_operation::is_abstract() const
        {
            return impl->is_abstract();
        }

        void uml_operation::is_abstract(bool value)
        {
            impl->is_abstract(value);
        }
    }
}