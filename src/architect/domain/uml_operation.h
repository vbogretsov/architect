#ifndef UML_OPERATION_H
#define UML_OPERATION_H

#include "uml_member.h"

namespace architect
{
    namespace domain
    {
        // Represents the class operation
        class uml_operation : public uml_member
        {
        public:
            // Creates new instance of uml_opration
            uml_operation(const std::string& name);
            // Deletes resources
            ~uml_operation();
            // Gets arguments list
            uts::iterable<uml_attribute_ptr> arguments() const;
            //Adds new argument
            void add(uml_attribute_ptr argument);
            // Removes argument
            void remove(uml_attribute_ptr argument);
            // Gets true if the operation is abstract, otherwise false
            bool is_abstract() const;
            // Sets abstract operation
            void is_abstract(bool value);
        private:
            USING_PIMPL(uml_operation)
        };

        typedef std::shared_ptr<uml_operation> uml_operation_ptr;
        typedef std::shared_ptr<const uml_operation> uml_operation_const_ptr;
    }
}

#endif