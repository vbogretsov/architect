#include "uml_package.h"
#include "uml_type.h"

namespace architect
{
    namespace domain
    {
        class uml_package::pimpl
        {
        public:

            const uml_package* parent() const
            {
                return _parent;
            }

            void parent(const uml_package* value)
            {
                _parent = value;
            }

            std::shared_ptr<const std::string> full_name(
                const std::string& delimiter,
                const std::string& name)
            {
                std::shared_ptr<std::string> full_name =
                    std::shared_ptr<std::string>(new std::string(name));
                uml_package const* p = _parent;
                while (p)
                {
                    *full_name = p->name() + delimiter + *full_name;
                    p = p->parent();
                }
                return full_name;
            }

            uts::iterable<uml_package_ptr> packages()
            {
                return uts::iterable<uml_package_ptr>(
                    _packages.begin(),
                    _packages.end());
            }

            void add(uml_package_ptr package)
            {
                _packages.push_back(package);
            }

            void remove(uml_package_ptr package)
            {
                _packages.remove(package);
            }

            uts::iterable<uml_type_ptr> types()
            {
                return uts::iterable<uml_type_ptr>(
                    _types.begin(),
                    _types.end());
            }

            void add(uml_type_ptr type)
            {
                _types.push_back(type);
            }

            void remove(uml_type_ptr type)
            {
                _types.remove(type);
            }

        private:

            std::list<uml_package_ptr> _packages;

            std::list<uml_type_ptr> _types;

            uml_package const* _parent;
        };

        shared_ptr<uml_package> uml_package::create(const std::string& name)
        {
            return shared_ptr<uml_package>(new uml_package(name));
        }

        uml_package::uml_package(const std::string& name)
            : uml_object(name), impl(new pimpl())
        {
        }

        uml_package::~uml_package()
        {
        }

        const uml_package* uml_package::parent() const
        {
            return impl->parent();
        }

        std::shared_ptr<const std::string> uml_package::full_name(
            const std::string& delimiter) const
        {
            return impl->full_name(delimiter, name());
        }

        void uml_package::parent(const uml_package* value)
        {
            impl->parent(value);
        }

        uts::iterable<uml_package_ptr> uml_package::packages() const
        {
            return impl->packages();
        }

        void uml_package::add(uml_package_ptr package)
        {
            impl->add(package);
            package->parent(this);
        }

        void uml_package::remove(uml_package_ptr package)
        {
            impl->remove(package);
        }

        uts::iterable<uml_type_ptr> uml_package::types() const
        {
            return impl->types();
        }

        void uml_package::add(uml_type_ptr type)
        {
            impl->add(type);
            //package(this);
            type->package(this);
        }

        void uml_package::remove(uml_type_ptr type)
        {
            impl->remove(type);
        }
    }
}