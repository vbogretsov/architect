#ifndef UML_PACKAGE_H
#define UML_PACKAGE_H

#include <list>

#include "../utils/iterable.h"
#include "uml_object.h"

namespace architect
{
    namespace domain
    {
        class uml_type;
        typedef std::shared_ptr<uml_type> uml_type_ptr;
        typedef std::shared_ptr<const uml_type> uml_type_const_ptr;
        class uml_package;
        typedef std::shared_ptr<uml_package> uml_package_ptr;
        typedef std::shared_ptr<const uml_package> uml_package_const_ptr;

        // Represents UML package
        class uml_package : public uml_object
        {
        public:
            // Creates new instance of uml_package
            static uml_package_ptr create(const std::string& name);
            // Creates new instance of uml_package
            uml_package(const std::string& name);
            // Deletes resources
            virtual ~uml_package();
            // Gets parent package
            const uml_package* parent() const;
            // Gets the full package name using specified delimiter
            std::shared_ptr<const std::string> full_name(const std::string& delimiter) const;
            // Gets collection of child packages
            uts::iterable<uml_package_ptr> packages() const;
            // Adds child package
            void add(uml_package_ptr package);
            // Removes child package from
            void remove(uml_package_ptr package);
            // Gets list of classes
            uts::iterable<uml_type_ptr> types() const;
            // Adds the class
            void add(uml_type_ptr type);
            // Removes the class
            void remove(uml_type_ptr type);
        private:
            // Sets parent package
            void parent(const uml_package* value);
        private:
            USING_PIMPL(uml_package)
        };
    }
}

#endif