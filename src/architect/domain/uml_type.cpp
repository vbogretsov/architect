#include "uml_type.h"

namespace architect
{
    namespace domain
    {
        class uml_type::pimpl
        {
        public:

            const uml_package* package() const
            {
                return _package;
            }

            void package(const uml_package* value)
            {
                _package = value;
            }

        private:

            uml_package const* _package;
        };

        uml_type::uml_type(const std::string& name)
            : uml_object(name), impl(new pimpl())
        {
        }

        uml_type::~uml_type()
        {
        }

        const uml_package* uml_type::package() const
        {
            return impl->package();
        }

        void uml_type::package(const uml_package* value)
        {
            impl->package(value);
        }
    }
}