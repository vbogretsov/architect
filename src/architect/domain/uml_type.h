#ifndef UML_TYPE_H
#define UML_TYPE_H

#include "uml_object.h"
#include "uml_package.h"

namespace architect
{
    namespace domain
    {
        // Represents data type
        class uml_type : public uml_object
        {
        friend uml_package;
        public:
            // Creates new instance of uml_type
            uml_type(const std::string& name);
            // Clears all resources
            virtual ~uml_type();
            // Gets the parent package
            const uml_package* package() const;
            // Sets the parent package
            void package(const uml_package* value);
        private:
            USING_PIMPL(uml_type)
        };

        // pointers are defined in uml_package.h
    }
}

#endif