#ifndef ITERABLE_H
#define ITERABLE_H

#include <iterator>
#include <memory>

namespace uts
{
    template <typename T>
    class iterator_base
    {
    public:
        virtual ~iterator_base() {}
        virtual T& operator*() = 0;
        virtual void operator++() = 0;
        virtual bool operator==(const iterator_base<T>& other) const = 0;
        virtual bool operator!=(const iterator_base<T>& other) const = 0;
    };

    template <typename T, typename I>
    class iterator_wrapper : public iterator_base<T>
    {
    public:
        iterator_wrapper(I i);
        T& operator*() override;
        void operator++() override;
        bool operator==(const iterator_base<T>& other) const override;
        bool operator!=(const iterator_base<T>& other) const override;
    private:
        I _i;
    };

    template <typename T>
    class iterator_provider
    {
    public:
        virtual ~iterator_provider() {}
        virtual std::shared_ptr<iterator_base<T>> create() const = 0;
    };

    template <typename T, typename I>
    class iterator_provider_wrapper : public iterator_provider<T>
    {
    public:
        iterator_provider_wrapper(I i);
        std::shared_ptr<iterator_base<T>> create() const override;
    private:
        I _i;
    };

    template <typename T>
    class iterator
    {
    public:
        typedef std::ptrdiff_t difference_type;
        typedef T value_type;
        typedef T* pointer;
        typedef T& reference;
        typedef std::forward_iterator_tag iterator_category;
    public:
        iterator(std::shared_ptr<iterator_base<T>> i);
        inline T& operator*();
        inline iterator<T>& operator++();
        inline bool operator==(const iterator& other) const;
        inline bool operator!=(const iterator& other) const;
    private:
        std::shared_ptr<iterator_base<T>> _i;
    };

    template <typename T>
    class iterable
    {
    public:
        template <typename I>
        iterable(I b, I e);
        iterator<T> begin();
        iterator<T> end();
    private:
        std::shared_ptr<iterator_provider<T>> _b;
        std::shared_ptr<iterator_provider<T>> _e;
    };

    template <typename T, typename I>
    iterator_wrapper<T, I>::iterator_wrapper(I i)
        : _i(i)
    {
    }

    template <typename T, typename I>
    T& iterator_wrapper<T, I>::operator*()
    {
        return *_i;
    }

    template <typename T, typename I>
    void iterator_wrapper<T, I>::operator++()
    {
        ++_i;
    }

    template <typename T, typename I>
    bool iterator_wrapper<T, I>::operator==(const iterator_base<T>& other) const
    {
        const iterator_wrapper<T, I>& o =
            dynamic_cast<const iterator_wrapper<T, I>&>(other);
        return _i == o._i;
    }

    template <typename T, typename I>
    bool iterator_wrapper<T, I>::operator!=(const iterator_base<T>& other) const
    {
        const iterator_wrapper<T, I>& o =
            dynamic_cast<const iterator_wrapper<T, I>&>(other);
        return _i != o._i;
    }

    template <typename T>
    iterator<T>::iterator(std::shared_ptr<iterator_base<T>> i)
        : _i(i)
    {
    }

    template <typename T>
    bool iterator<T>::operator==(const iterator& other) const
    {
        return *_i == *(other._i);
    }

    template <typename T>
    bool iterator<T>::operator!=(const iterator& other) const
    {
        return *_i != *(other._i);
    }

    template <typename T>
    iterator<T>& iterator<T>::operator++()
    {
        ++(*_i);
        return *this;
    }

    template <typename T>
    T& iterator<T>::operator*()
    {
        return **_i;
    }

    template <typename T, typename I>
    iterator_provider_wrapper<T, I>::iterator_provider_wrapper(I i)
        : _i(i)
    {
    }

    template <typename T, typename I>
    std::shared_ptr<iterator_base<T>> iterator_provider_wrapper<T, I>::create() const
    {
        return std::shared_ptr<iterator_base<T>>(new iterator_wrapper<T, I>(_i));
    }

    template <typename T>
    template <typename I>
    iterable<T>::iterable(I b, I e)
        : _b(new iterator_provider_wrapper<T, I>(b)),
          _e(new iterator_provider_wrapper<T, I>(e))
    {
    }

    template <typename T>
    iterator<T> iterable<T>::begin()
    {
        return iterator<T>(_b->create());
    }

    template <typename T>
    iterator<T> iterable<T>::end()
    {
        return iterator<T>(_e->create());
    }
}

#endif