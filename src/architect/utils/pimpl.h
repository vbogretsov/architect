#ifndef PIMPL_H
#define PIMPL_H

#include <memory>

#define USING_PIMPL(type) \
    class pimpl; \
    std::unique_ptr<pimpl> impl; \

#define DISABLE_COPY(type) \
    type(const type& other) delete; \
    type& operator=(const type& other) delete; \

#define ENABLE_COPY(type) \
    type(const type& other); \
    type& operator=(const type& other); \

#define PIMPL_COPY(type) \
    type::type(const type& other) \
    : impl(new pimpl(*(other.impl))) \
    { \
    } \
    \
    type& type::operator=(const type& other) \
    { \
        impl.reset(new pimpl(*(other.impl))); \
        return *this; \
    } \

#define PIMPL_COMPARE(type) \
    bool type::operator==(const type& other) const\
    { \
        return *impl == *(other.impl); \
    } \
    \
    bool type::operator!=(const type& other) const\
    { \
        return *impl != *(other.impl); \
    } \

#endif