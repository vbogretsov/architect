#ifndef TREE_H
#define TREE_H

#include <stack>
#include <functional>

#include "iterable.h"

namespace uts
{
    template <typename T>
    class tree_visitor
    {
    public:
        virtual ~tree_visitor() {}
        virtual void down() = 0;
        virtual void visit(T& t) = 0;
        virtual void up() = 0;
    };

    template <typename T>
    class tree
    {
    public:
        class iterator
        {
        public:
            typedef std::ptrdiff_t difference_type;
            typedef T value_type;
            typedef T* pointer;
            typedef T& reference;
            typedef std::forward_iterator_tag iterator_category;
        public:
            iterator();
            iterator(T& root, std::function<iterable<T>(T&)> children);
            bool operator==(const iterator& other) const;
            bool operator!=(const iterator& other) const;
            T& operator*();
            iterator& operator++();
        private:
            std::function<iterable<T>(T&)> _children;
            std::stack<T*> _stack;
        };
    public:
        tree(T& root, std::function<iterable<T>(T&)> children);
        iterator begin();
        iterator end();
        void bypass(tree_visitor<T>* visitor);
    private:
        void visit(tree_visitor<T>* visitor, T& node);
    private:
        T& _root;
        std::function<iterable<T>(T&)> _children;
    };

    template <typename T>
    tree<T>::tree(T& root, std::function<iterable<T>(T&)> children)
        : _root(root), _children(children)
    {
    }

    template <typename T>
    typename tree<T>::iterator tree<T>::begin()
    {
        return iterator(_root, _children);
    }

    template <typename T>
    typename tree<T>::iterator tree<T>::end()
    {
        return iterator();
    }

    template <typename T>
    void tree<T>::bypass(tree_visitor<T>* visitor)
    {
        visit(visitor, _root);
    }

    template <typename T>
    void tree<T>::visit(tree_visitor<T>* visitor, T& node)
    {
        visitor->visit(node);
        visitor->down();
        iterable<T> children = _children(node);
        for each (T& child in children)
        {
            visit(visitor, child);
        }
        visitor->up();
    }

    template <typename T>
    tree<T>::iterator::iterator()
    {
    }

    template <typename T>
    tree<T>::iterator::iterator(T& root, std::function<iterable<T>(T&)> children)
        : _children(children)
    {
        _stack.push(&root);
    }

    template <typename T>
    bool tree<T>::iterator::operator==(typename const tree<T>::iterator& other) const
    {
        return _stack == other._stack;
    }

    template <typename T>
    bool tree<T>::iterator::operator!=(typename const tree<T>::iterator& other) const
    {
        return !(*this == other);
    }

    template <typename T>
    T& tree<T>::iterator::operator*()
    {
        return *(_stack.top());
    }

    template <typename T>
    typename tree<T>::iterator& tree<T>::iterator::operator++()
    {
        T* current = _stack.top();
        _stack.pop();
        iterable<T> children = _children(*current);
        if (children.begin() != children.end())
        {
            for each (T& child in children)
            {
                _stack.push(&child);
            }
        }
        return *this;
    }
}

#endif