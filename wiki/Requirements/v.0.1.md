# Version 01

## Requirements

  1. Create class diagram model.
  1. namespaces.
  2. interfaces (methods).
  3. classes (methods, attributes).
  4. enums (literals).
  5. visibility (private, protected, namespace, public).
  6. inheritance (class, interface).
  7. aggregation (quantity).
  8. composition (quantity).
  9. dependency (create, use).
  11. association (quantity).
  12. coloboration through (interface, class).
  13. templates (constraints).

## Scenarios

### Model

1. User creates new model.
2. User renames new model.

### Namespaces

1. Add namespace into model.
2. Rename namespace.
3. Add child namespace into namespace.
4. Rename namespace.
5. Get list of namespaces in model/namespace
6. Move namespace in other namespace or in root.
7. Get list of classes in namespace (not in child namespaces)
8. Get list of classes in namespace and its childs.

### Classes